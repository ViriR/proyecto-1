PROYECTO 1; 10/09/2019


El proyecto #1 de la materia de Dise�o y An�lisis de Algoritmos consisti� en la construcci�n 
aleatoria de grafos bajo cuatro diferentes m�todos (Erd�s y R�nyi, Gilbert, Geogr�fico simple, 
y Barab�si-Albert); mismo que fue desarrollado en Java.


A continuaci�n se describir� de manera concisa el procedimiento a realizar para la generaci�n
de cualquier tipo de grafo:

1.- Abrir en cualquier editor de Java (el proyecto se realiz� en NetBeans IDE) el proyecto 
titulado "Proyecto 1".
2.- Dentro del proyecto podr�n ser visualizadas tres clases: Grafos, Aristas, Nodos. Abrir la
clase "Grafos".
3.- En la clase "Grafos" se encuentra desarrollado el algoritmo pertinente para la generaci�n de
cualquier tipo de grafo (Erd�s y R�nyi, Gilbert, Geogr�fico simple, y Barab�si-Albert). En la parte
final se puede observar el m�todo "main", donde se da la instrucci�n de construir el grafo deseado
y colocarle un nombre al archivo .gv.
4.- Una vez generado el grafo dentro de un arvhivo .gv, podr� ser observado de manera gr�fica a 
trav�s del software Gephi, en donde se abre el archivo correspondiente y el grafo puede ser 
visualizado.

- Viridiana Rodr�guez Gonz�lez 