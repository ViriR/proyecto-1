package grafo;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;

public class Grafos {

    private HashMap<Integer, Nodos> Nodos;
    private HashMap<Integer, Aristas> Aristas;

    public Grafos(HashMap<Integer, Nodos> N, HashMap<Integer, Aristas> A) {
        this.Aristas = new HashMap();
        for (int i = 0; i < A.size(); i++) {
            this.Aristas.put(i, new Aristas(A.get(i)));
        }
        this.Nodos = new HashMap();
        for (int i = 0; i < N.size(); i++) {
            this.Nodos.put(i, new Nodos(N.get(i)));
        }
    }

    public Grafos() {
        this.Aristas = new HashMap();
        this.Nodos = new HashMap();
    }

    public Grafos(Grafos k) {
        this.Aristas = new HashMap();
        for (int i = 0; i < k.getAristas().size(); i++) {
            this.Aristas.put(i, new Aristas(k.getAristas().get(i)));
        }
        this.Nodos = new HashMap();
        for (int i = 0; i < k.getNodos().size(); i++) {
            this.Nodos.put(i, new Nodos(k.getNodos().get(i)));
        }
    }

    public void setNodos(HashMap<Integer, Nodos> w) {
        this.Nodos = w;
    }

    public void setAristas(HashMap<Integer, Aristas> w) {
        this.Aristas = w;
    }

    public HashMap<Integer, Aristas> getAristas() {
        return this.Aristas;
    }

    public HashMap<Integer, Nodos> getNodos() {
        return this.Nodos;
    }

    
    public static Grafos ErdosRenyi(int NumNodos, int NumAristas, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int AristasHechas;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        int x1 = (int) (Math.random() * NumNodos), x2 = (int) (Math.random() * NumNodos);
        AristaS.put(0, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));

        while (x1 == x2 && dirigido == 0) {
            x1 = (int) (Math.random() * NumNodos);
            x2 = (int) (Math.random() * NumNodos);
            AristaS.put(0, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));
        }

        NodoS.get(x1).conectar();
        NodoS.get(x2).conectar();
        if (x1 != x2) {
            NodoS.get(x1).IncGrado(1);
        }
        NodoS.get(x2).IncGrado(1);

        AristasHechas = 1;
        while (AristasHechas < NumAristas) {
            x1 = (int) (Math.random() * NumNodos);
            x2 = (int) (Math.random() * NumNodos);

            if (x1 != x2 || dirigido == 1) {
                int c1 = 1, cont = 0;
                while (c1 == 1 && cont < AristasHechas) {
                    int a = AristaS.get(cont).getAn1(), b = AristaS.get(cont).getAn2();
                    if ((x1 == a && x2 == b) || (x1 == b && x2 == a)) {
                        c1 = 0;
                    }
                    cont++;
                }
                if (c1 == 1) {
                    AristaS.put(AristasHechas, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));
                    NodoS.get(x1).conectar();
                    NodoS.get(x2).conectar();
                    if (x1 != x2) {
                        NodoS.get(x1).IncGrado(1);
                    }
                    NodoS.get(x2).IncGrado(1);
                    AristasHechas++;
                }
            }
        }

       
        Grafos G = new Grafos(NodoS, AristaS);
        return G;

    }
    
    public static Grafos Geografico(int NumNodos, double distancia, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();
        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i, Math.random(), Math.random()));
        }

        for (int i = 0; i < NumNodos; i++) {
            for (int j = i; j < NumNodos; j++) {
                if (j != i || dirigido == 1) {
                    double dis = Math.sqrt(Math.pow(NodoS.get(j).getX() - NodoS.get(i).getX(), 2)
                            + Math.pow(NodoS.get(j).getY() - NodoS.get(i).getY(), 2));
                    if (dis <= distancia) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).IncGrado(1);
                        NodoS.get(i).conectar();
                        if (j != i) {
                            NodoS.get(j).IncGrado(1);
                            NodoS.get(j).conectar();
                        }
                        NumAristas++;
                    }
                }
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static Grafos Gilbert(int NumNodos, double probabilidad, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        for (int i = 0; i < NumNodos; i++) {
            for (int j = i; j < NumNodos; j++) {
                if (j != i || dirigido == 1) {
                    if (Math.random() <= probabilidad) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).conectar();
                        NodoS.get(j).conectar();
                        if (i != j) {
                            NodoS.get(i).IncGrado(1);
                        }
                        NodoS.get(j).IncGrado(1);
                        NumAristas++;
                    }
                }
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static Grafos Barabasi(int NumNodos, double d, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        for (int i = 0; i < NumNodos; i++) {
            int j = 0;
            while (j <= i && NodoS.get(i).getGrado() <= d) {
                if (j != i || dirigido == 1) {
                    if (Math.random() <= 1 - NodoS.get(j).getGrado() / d) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).IncGrado(1);
                        NodoS.get(i).conectar();
                        if (j != i) {
                            NodoS.get(j).IncGrado(1);
                            NodoS.get(j).conectar();
                        }
                        NumAristas++;
                    }
                }
                j++;
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static void construir(String nombre, Grafos g) {
        FileWriter fichero = null;
        PrintWriter pw = null;

        System.out.println(g.getNodos().size());

        try {
            fichero = new FileWriter(nombre + ".gv");
            pw = new PrintWriter(fichero);
            pw.println("graph 666{"); //Por Gephi
            for (int i = 0; i < g.getNodos().size(); i++) {
                pw.println(g.getNodos().get(i).get() + "  " + "[Label = \"" + g.getNodos().get(i).get() + " (" + String.format("%.2f", g.getNodos().get(i).getwin()) + ")\"]");
            }
            pw.println();
            for (int i = 0; i < g.getAristas().size(); i++) {
                pw.println(g.getAristas().get(i).getAn1() + "--" + g.getAristas().get(i).getAn2() + "  " + "[Label = \"" + String.format("%.2f", g.getAristas().get(i).getP()) + "\"]");
            }
            pw.println("}");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public static void main(String[] args) {

        Grafos GN = new Grafos();
        GN = ErdosRenyi(30,57,0);
        //GN= Geografico(500,.3,0);
        //GN = Gilbert(100,.55,0);
        //GN = Barabasi(500,1060,0);  
        construir("grafoER3",GN);
    }
}
